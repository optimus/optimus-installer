<?php
include('/srv/api/config.php');
$current_time = date('Y-m-d_H-i-s');
exec('mkdir /srv/migration');
file_put_contents('/srv/migration/' . $current_time . '.log', $current_time . "\n");
file_put_contents('/srv/migration/' . $current_time . '_errors.log', $current_time . "\n");

function clog($string, $type = 39) 
{
	global $current_time;
	$colors = 
	[
		'red' => 31,
		'green' => 32,
		'yellow' => 33,
		'blue' => 34,
		'magenta' => 35,
		'cyan' => 36,
	];
	$color = $colors[$type] ?? 0;
	echo "\033[".$color."m".$string."\033[0m\n";
	file_put_contents('/srv/migration/' . $current_time . '.log', $string . "\n", FILE_APPEND);
}

function rest($endpoint, $method, $input, $token)
{
	global $current_time;
	if ($method == "GET" AND isset($input))
		$endpoint .= "?data=" . urlencode($input);

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $endpoint);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Cookie: token=" . $token));
	if ($method == "POST")
		curl_setopt($curl, CURLOPT_POST, true);
	else if ($method == "PATCH")
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
	else if ($method == "DELETE")
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
	if ($method != "GET" AND isset($input))
		curl_setopt($curl, CURLOPT_POSTFIELDS, $input);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($curl, CURLOPT_TIMEOUT, 5);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($curl, CURLOPT_HEADER, true);
	curl_setopt($curl, CURLOPT_FRESH_CONNECT, TRUE);
	$response = curl_exec($curl);
	$response = explode("\r\n\r\n", $response);
	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (sizeof($response) > 1)
	{
		$header = $response[0];
		$body = json_decode($response[1]);

		preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
		$body->cookies = new stdClass;
		foreach($matches[1] as $item)
		{
			$item = explode('=', $item);
			$body->cookies->{$item[0]} = $item[1];
		}
	}
	else
		$body = json_decode($response[0]);

	if ($body->code < 400)
		clog("rest('" . $endpoint . "', '"  . strtoupper($method) . "'" . ($input ? ", " . $input : "") . ")", "green");
	else
	{
		clog("rest('" . $endpoint . "', '"  . strtoupper($method) . "'" . ($input ? ", " . $input : "") . ")", "red");
		file_put_contents('/srv/migration/' . $current_time . '_errors.log', "rest('" . $endpoint . "'" . ($input ? ", " . $input : "") . ")\n", FILE_APPEND);
		clog("--> ERROR : " .  $body->code . ' : ' . $body->message, "red");
		file_put_contents('/srv/migration/' . $current_time . '_errors.log', "--> ERROR : " .  $body->code . ' : ' . $body->message . "\n", FILE_APPEND);
		clog("Appuyez une touche pour continuer, ou CTRL+C pour stopper le script... \n");
		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
	}
	
	if (curl_errno($curl))
		return (object)array("code" => ($http_status ? $http_status : 400), "message" => curl_error($curl));
	else
		return $body;
}

function ssh($command)
{
	global $destination_host;
	exec('ssh debian@' . $destination_host . ' -p 7822 -i /root/.ssh/id_rsa ' . '"' . $command . '"', $output);
	return $output;
}

system("clear");
echo "\n";
clog("==== MIGRATION OPTIMUS V4 -> V5 ====\n", "green");
clog("Attention : les données présentes sur le serveur de destination vont être supprimées (fichiers, courriels, contacts, dossiers, agendas)\n", "red");
clog("Indiquez le nom de domaine ou l'adresse ip du serveur vers lequel migrer les données (par défaut " . $domain . ") : \n", "magenta");

$handle = fopen ("php://stdin","r");
$line = fgets($handle);
$destination_host = strlen($line) == 1 ? $domain : trim($line);

//CONNEXION SSH
if (!file_exists('/root/.ssh/id_rsa'))
{
	echo "\n";
	clog("Génération d'une paire de clé RSA", "magenta");
	exec("ssh-keygen -f /root/.ssh/id_rsa -N ''");
	if (file_exists('/root/.ssh/id_rsa'))
		clog("Les clés ont été générées avec succès\n", "green");
	else
		die(clog("Les clés n'ont pas pu être générées\n", "red"));
}

echo "\n";
clog("Copie de la clé publique sur le serveur distant", "magenta");
exec('ssh-copy-id -p 7822 -o StrictHostKeyChecking=no debian@' . $destination_host . ' > /dev/null 2>&1');

if (ssh('whoami')[0] == 'debian')
	clog("Connexion SSH avec le serveur distant établie !\n", "green");
else
	die(clog("La connexion SSH avec le serveur distant n'a pas pu être établie\n", "red"));


//ON VERIFIE QUE LE SERVEUR NE TENTE PAS DE SE CONNECTER A LUI MËME
if (exec('hostname') == ssh('hostname')[0])
	die(clog("L'ancien serveur tente de se connecter à lui même. Il est possible que les DNS n'aient pas encore eu le temps de se mettre à jour. Ré-essayez dans quelques minutes.\n", "red"));


//LECTURE DU FICHIER DE CONFIG DISTANT
$remote_config_lines = ssh('sudo cat /root/.optimus');
foreach($remote_config_lines as $line)
	if (stripos($line, 'export ') !== false)
		$remote_config[explode('=', str_replace('export ','',$line))[0]] = explode('=', $line)[1];


//CONNEXION MARIADB
clog("==CONNEXION AUX BASES DE DONNEES==", "magenta");
$old = new PDO("mysql:host=localhost", "root");
if ($old) 
	clog("Connexion au serveur MARIADB V4 effectuée", "green");
$new = new PDO("mysql:host=optimus." . $destination_host . ";port:3306", 'root', $remote_config['MARIADB_ROOT_PASSWORD']);
if ($new) 
	clog("Connexion au serveur MARIADB V5 effectuée", "green");


//MYSQL DUMP
clog("\n==BACKUP DE LA BASE V4==", "magenta");
system('mysqldump --all-databases --ignore-table=mailserver.bayes_token  --ignore-table=mailserver.bayes_seen --extended-insert=FALSE > "/tmp/' . $current_time . '.sql"', $dump);
if ($dump == 0)
	clog('Fichier /srv/migration/' . $current_time . '.sql créé avec succès', 'green');
else
	clog('Code erreur : ' . $dump, 'red');


//SUPPRESSION DES FICHIERS DE DESTINATION
ssh('sudo rm -f -R /srv/files/*');


//CONNEXION API
clog("\n==RECUPERATION D'UN TOKEN JWT POUR CONNEXION A L'API EN MODE ADMIN==", "magenta");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE server.users");
$new->query("SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO'");
$new->query("INSERT INTO server.users SET id = 0, email = 'tempadmin@" . $destination_host . "', status = b'1', admin = b'1', firstname = 'TEMP', lastname = 'ADMIN', password = '" . password_hash('optim1gr4ate', PASSWORD_BCRYPT) . "'");
$new->query("SET SQL_MODE=''");
$get_token = rest('https://api.' . $destination_host . '/optimus-base/login', 'POST', '{"email":"tempadmin@' . $destination_host . '", "password":"optim1gr4ate"}', null);
if ($get_token->cookies)
{
	$token = $get_token->cookies->token;
	clog("Token : " . $token, "green");
}
else
	die(clog("Récupération du token échouée", "red"));

	
//AJOUT DES UTILISATEURS
clog("\n==AJOUT DES UTILISATEURS ET ACTIVATION DES SERVICES==", "magenta");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE server.users_services");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE server.businesses");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE mailserver.aliases");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE mailserver.redirections");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE mailserver.sender_bcc");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE mailserver.recipient_bcc");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE mailserver.quota");
$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE mailserver.transport");
for ($i=0; $i<99; $i++)
	$new->query("DROP DATABASE user_" . $i);
$old_users = $old->query("SELECT id, email, status, admin, firstname, lastname, AES_DECRYPT(password, '" . $aes_key . "') AS password FROM server.users ORDER BY id")->fetchAll(PDO::FETCH_OBJ);
foreach($old_users as &$user)
{
	clog($user->email, "cyan");
	
	$is_user = $old->query("SELECT * FROM information_schema.tables WHERE table_schema = '" . $user->email . "' AND table_name = 'dossiers'")->rowCount();
	
	if ($is_user == 1)
	{
		$user->type = 1;
		$user->status = (int)$user->status = 1;
		$user->admin = (int)$user->admin = 1;
		$new_user = rest('https://api.' . $destination_host . '/optimus-base/users', 'POST', json_encode($user), $token);
		$user->old_id = $user->id;
		$user->old_email = $user->email;
		$user->id = $new_user->data->id;

		$services = ["optimus-contacts", "optimus-calendar", "optimus-drive", "optimus-business", "optimus-avocats", "optimus-mail", "optimus-webmail"];
		foreach ($services as $service)
			rest('https://api.' . $destination_host . '/' . $service . '/' . $user->id . '/service', 'POST', null, $token);

		$users[] = $user;
	}
}


//AJOUT DES ENTREPRISES
clog("\n==AJOUT DES ENTREPRISES==", "magenta");
$old_users = $old->query("SELECT id, email, status, admin, firstname, lastname, AES_DECRYPT(password, '" . $aes_key . "') AS password FROM server.users ORDER BY id")->fetchAll(PDO::FETCH_OBJ);
foreach($old_users as &$user)
{
	clog($user->email, "cyan");

	$is_structure = $old->query("SELECT * FROM information_schema.tables WHERE table_schema = '" . $user->email . "' AND table_name = 'associes'")->rowCount();

	if ($is_structure == 1)
	{
		$business = new stdClass();
		$business->email = 'cabinet_' . $user->email;
		$business->name = "CABINET " . $user->firstname . " " . $user->lastname;
		$new_business = rest('https://api.' . $destination_host . '/optimus-business/businesses', 'POST', json_encode($business), $token);

		$business_user = rest('https://api.' . $destination_host . '/optimus-base/users/' . $new_business->data->id, 'GET', null, $token)->data;
		$business_user->old_id = $user->id;
		$business_user->old_email = $user->email;

		foreach($users as $existing_user)
			if($business_user->old_id == $existing_user->id)
				$business_user->replicant = true;

		$businesses[] = $business_user;
	}	
}


//AJOUT DES ASSOCIES DES ENTREPRISES
clog("\n==AJOUT DES ASSOCIES DES ENTREPRISES==", "magenta");
foreach($businesses as &$business)
{
	clog($business->email, "cyan");
	
	$business->partners = array();
	$partners = $old->query("SELECT * FROM `" . $business->old_email . "`.associes ORDER BY id")->fetchAll(PDO::FETCH_OBJ);
	foreach($partners as $partner)
	{
		$partner->server = $destination_host;
		foreach($users as $user)
			if ($user->old_id == $partner->user)
				$partner->user = (int)$user->id;
		if (!$partner->entree)
			$partner->arrival = date('Y') . '-01-01';
		else
			$partner->arrival = $partner->entree;
		$partner->departure = $partner->sortie;
		$new_partner = rest('https://api.' . $destination_host . '/optimus-business/' . $business->id . '/partners', 'POST', json_encode($partner), $token);
		$business->partners[] = $new_partner->data;
	}
}


//RECUPERATION DES REGLAGES DE COURRIELS (ALIAS, REDIRECTIONS ...)
clog("\n==CONFIGURATION DU COURRIEL==", "magenta");
$mail_users = array_merge($users, $businesses);
foreach($mail_users as $user)
{
	if (!isset($user->replicant))
	{
		$mailbox = $old->query("SELECT * FROM mailserver.mailboxes WHERE email = '" . $user->old_email . "'")->fetch(PDO::FETCH_OBJ);
			
		$aliases = explode(';', $mailbox->aliases);
		foreach($aliases as $alias)
			if (stripos($alias, '@'))
				if (explode('@', $alias)[0] == 'postmaster')
					rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-postmaster/' . $alias, 'POST', null, $token);
				else
					rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-aliases/' . $alias, 'POST', null, $token);
	
		$redirections = explode(';', $mailbox->redirections);
			foreach($redirections as $redirection)
				if (stripos($redirection, '@'))	
					rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-redirections/' . $redirection, 'POST', null, $token);
	
		$sender_bccs = explode(';', $mailbox->sender_bcc);
		foreach($sender_bccs as $sender_bcc)
			if (stripos($sender_bcc, '@'))	
				rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-sender_bcc/' . $sender_bcc, 'POST', null, $token);
	
		$recipient_bccs = explode(';', $mailbox->recipient_bcc);
		foreach($recipient_bccs as $recipient_bcc)
			if (stripos($recipient_bcc, '@'))	
				rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-recipient_bcc/' . $recipient_bcc, 'POST', null, $token);
	
		if ($mailbox->quota)
			rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-quota/' . $mailbox->quota, 'PATCH', null, $token);
			
		if ($mailbox->transport)
			rest('https://api.' . $destination_host . '/optimus-mail/' . $user->id . '/mailbox-transport/' . $mailbox->transport, 'POST', null, $token);
	}
}


//RECUPERATION DES DONNEES DES UTILISATEURS
$contacts = array();
$dossiers = array();
foreach($users as &$user)
{
	clog($user->email, 'cyan');

	//CONTACTS
	clog("\n==RECUPERATION DES CONTACTS==", "magenta");
	$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE `user_" . $user->id . "`.contacts");
	$contacts_categories = array('10' => 10, '30' => 30, '50' => 50, '60' => 60, '70' => 70, '110' => 110, '120' => 120, '255' => 255);
	$contacts_types = array('10' => 25, '20' => 25, '25' => 25, '30' => 30, '40' => 40);
	$old_contacts = $old->query("SELECT * FROM `" . $user->email . "`.contacts WHERE id != 0 ORDER by id")->fetchAll(PDO::FETCH_OBJ);
	foreach($old_contacts as &$contact)
	{
		$contact->categorie = in_array($contacts_categories, array_keys($contacts_categories)) ? $contacts_categories[$contact->categorie] : 0;
		$contact->type = in_array($contact->type, array_keys($contacts_types)) ? $contacts_types[$contact->type] : 0;
		$contact->birth_country = (int)$contact->birth_country;
		$contact->marital_status = (int)$contact->marital_status;
		$contact->company_type = (int)$contact->company_type;
		$contact->siren = $contact->siret; unset($contact->siret);
		$contact->company_status = 0;
		$contact->email = strtolower(trim($contact->email));
		$contact->pro_email = strtolower(trim($contact->pro_email));
		$contact->country = (int)$contact->country;
		$response = rest('https://api.' . $destination_host . '/optimus-contacts/' . $user->id . '/contacts', 'POST', json_encode($contact), $token);
		if ($response->code == 201)
		{
			$response->data->old_id = $contact->id;
			$contacts[$user->id][] = $response->data;
		}
	}

	//DOSSIERS
	clog("\n==RECUPERATION DES DOSSIERS==", "magenta");
	$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE `user_" . $user->id . "`.dossiers_intervenants");
	$old_dossiers = $old->query("SELECT * FROM `" . $user->email . "`.dossiers WHERE id != 0 ORDER by id")->fetchAll(PDO::FETCH_OBJ);
	foreach($old_dossiers as &$dossier)
	{
		$dossier->numero_archive = (int)$dossier->numero_archive;
		$dossier->domaine = !$dossier->domaine ? 0 : (int)$dossier->domaine;
		$dossier->sous_domaine = !$dossier->sous_domaine ? 0 : (int)$dossier->sous_domaine;
		$dossier->conseil = (int)$dossier->conseil;
		$dossier->contentieux = (int)$dossier->contentieux;
		$dossier->aj = (int)$dossier->aj;
		$response = rest('https://api.' . $destination_host . '/optimus-avocats/' . $user->id . '/dossiers', 'POST', json_encode($dossier), $token);
		if ($response->code == 201)
		{
			$response->data->old_id = $dossier->id;
			$dossiers[$user->id][] = $response->data;
		}
	}

	//INTERVENANTS
	clog("\n==RECUPERATION DES INTERVENANTS==", "magenta");
	$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE `user_" . $user->id . "`.dossiers_intervenants");
	$old_intervenants = $old->query("SELECT * FROM `" . $user->old_email . "`.dossiers_intervenants ORDER BY lien, id")->fetchAll(PDO::FETCH_OBJ);
	$intervenants_qualites = array("10"=>10, "20"=>20, "21"=>30, "22"=>40, "23"=>50, "24"=>60, "25"=>70, "26"=>80, "27"=>90, "50"=>100);
	$intervenants_mandataires_types = array("30"=>10, "35"=>20, "40"=>30, "60"=>60, "70"=>70, "80"=>80, "90"=>90, "100"=>230, "110"=>120, "115"=>125, "120"=>130, "130"=>150, "140"=>160, "150"=>190, "160"=>200, "170"=>210);
	$intervenants = array();
	foreach($old_intervenants as &$intervenant)
	{
		foreach($contacts[$user->id] as $contact)
			if ($contact->old_id == $intervenant->contact)
				$intervenant->contact = (int)$contact->id ;
		foreach($dossiers[$user->id] as $dossier)
			if ($dossier->old_id == $intervenant->dossier)
				$dossier_id = (int)$dossier->id;
		if (!array_key_exists((int)$dossier_id, $intervenants))
			$intervenants[(int)$dossier_id] = array();
		
		if ($intervenant->lien == 0)
		{
			if (!array_key_exists((int)$intervenant->contact, $intervenants[(int)$dossier_id]))
				$intervenants[(int)$dossier_id][(int)$intervenant->contact] = (object)array
				(
					"contact" => (int)$intervenant->contact,
					"qualites" => array(),
					"mandataires" => array()
				);
			if (in_array($intervenant->qualite, array_keys($intervenants_qualites)))
				$intervenants[(int)$dossier_id][(int)$intervenant->contact]->qualites[] = $intervenants_qualites[$intervenant->qualite];
		}
		else
		{
			if (!array_key_exists((int)$intervenant->lien, $intervenants[(int)$dossier_id]))
				$intervenants[(int)$dossier_id][(int)$intervenant->lien] = (object)array
				(
					"contact" => (int)$intervenant->lien,
					"qualites" => array(),
					"mandataires" => array()
				);
			if (in_array($intervenant->qualite, array_keys($intervenants_mandataires_types)))
				$intervenants[(int)$dossier_id][(int)$intervenant->lien]->mandataires[] = (object)array("contact"=> (int)$intervenant->contact, "type"=> $intervenants_mandataires_types[$intervenant->qualite]);
		}
	}
	foreach($intervenants as $dossier_id => $intervenants)
		foreach($intervenants as $intervenant)
			rest('https://api.' . $destination_host . '/optimus-avocats/' . $user->id . '/dossiers/' . $dossier_id . '/intervenants', 'POST', json_encode($intervenant), $token);

	//DILIGENCES
	clog("\n==RECUPERATION DES DILIGENCES==", "magenta");
	$new->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE `user_" . $user->id . "`.dossiers_diligences");
	$old_diligences = $old->query("SELECT * FROM `" . $user->email . "`.dossiers_interventions_diligences WHERE id != 0 ORDER by id")->fetchAll(PDO::FETCH_OBJ);
	foreach($old_diligences as &$diligence)
	{
		$intervention = $old->query("SELECT * FROM `" . $user->email . "`.dossiers_interventions WHERE id = '" . $diligence->intervention . "'")->fetch(PDO::FETCH_OBJ);
		
		unset($diligence->intervention);

		foreach($dossiers[$user->id] as $dossier)
			if ($dossier->old_id == $intervention->dossier)
				$diligence->dossier = (int)$dossier->id;

		foreach($users as $existing_user)
			if($diligence->intervenant == $existing_user->old_id)
				$diligence->intervenant = (int)$existing_user->id;
		
		foreach($users as $existing_user)
			if($diligence->beneficiaire == $existing_user->old_id)
				$diligence->beneficiaire = (int)$existing_user->id;
		
		$diligence->commission = floatval($diligence->commission);

		$diligence->category = (int)$diligence->categorie;
		$diligence->type = (int)$diligence->type;

		$diligence->quantity = floatval($diligence->coefficient);
		$diligence->price = floatval($diligence->tarif);

		$response = rest('https://api.' . $destination_host . '/optimus-avocats/' . $user->id . '/dossiers/' . $diligence->dossier . '/diligences', 'POST', json_encode($diligence), $token);
		if ($response->code == 201)
		{
			$response->data->old_id = $diligence->id;
			$diligences[$user->id][] = $response->data;
		}
	}
}


//FACTURES
clog("\n==RECUPERATION DES FACTURES==", "magenta");
$tva_rates = array("1"=>10, "2"=>20, "3"=>30, "4"=>40, "5"=>50, "6"=>60, "7"=>70);
$lespays = $new->query("SELECT * FROM common.pays")->fetchAll(PDO::FETCH_OBJ);
foreach($lespays as $pays)
	$countries[$pays->id] = $pays->value;

for($i=0; $i < sizeof($businesses); $i++)
{
	clog($businesses[$i]->email, "cyan");
	$old_factures = $old->query("SELECT * FROM `" . $businesses[$i]->old_email . "`.factures WHERE id != 0 ORDER by id")->fetchAll(PDO::FETCH_OBJ);
	foreach($old_factures as $facture)
	{
		$owner = null;
		foreach($users as $user)
			if ($user->old_id == $facture->owner)
				$owner = $user;
		
		$contact = null;
		foreach($contacts[$owner->id] as $contact)
			if ($contact->old_id == $facture->client)
			{
				$client = rest('https://api.' . $destination_host . '/optimus-contacts/' . $owner->id . '/contacts/' . $contact->id, 'GET', null, $token)->data;
				if (!$client->zipcode OR !$client->city_name OR !$client->address)
					file_put_contents('/srv/migration/' . $current_time . '_errors.log', "Facture n°" . $facture->numero . "\n", FILE_APPEND);
				if (!$client->zipcode)
				{
					rest('https://api.' . $destination_host . '/optimus-contacts/' . $owner->id . '/contacts/' . $client->id, 'PATCH', '{"zipcode":"99999"}', $token)->data;
					file_put_contents('/srv/migration/' . $current_time . '_errors.log', "--> MODIFICATION : Le code postal du client étant absent, la valeur a été fixée à '99999'\n", FILE_APPEND);
				}
				if (!$client->city_name)
				{
					rest('https://api.' . $destination_host . '/optimus-contacts/' . $owner->id . '/contacts/' . $client->id, 'PATCH', '{"city_name":"NON RENSEIGNE"}', $token)->data;
					file_put_contents('/srv/migration/' . $current_time . '_errors.log', "--> MODIFICATION : La ville du client n'étant pas renseignée, la valeur a été fixée à 'NON RENSEIGNE'\n", FILE_APPEND);
				}
				if (!$client->address)
				{
					rest('https://api.' . $destination_host . '/optimus-contacts/' . $owner->id . '/contacts/' . $client->id, 'PATCH', '{"address":"NON RENSEIGNEE"}', $token)->data;
					file_put_contents('/srv/migration/' . $current_time . '_errors.log', "--> MODIFICATION : L'adresse du client n'étant pas renseignée, la valeur a été fixée à 'NON RENSEIGNEE'\n", FILE_APPEND);
				}
			}
		
		$dossier = null;
		foreach($dossiers[$owner->id] as $dossier)
			if ($dossier->old_id == $facture->dossier)
				$reference = rest('https://api.' . $destination_host . '/optimus-avocats/' . $owner->id . '/dossiers/' . $dossier->id, 'GET', null, $token)->data;
		
		$intervention = $old->query("SELECT * FROM `" . $owner->email . "`.dossiers_interventions WHERE id = '" . $facture->intervention . "'")->fetch(PDO::FETCH_OBJ);

		$diligences_id = array();
		$old_diligences = $old->query("SELECT * FROM `" . $owner->email . "`.dossiers_interventions_diligences WHERE intervention = '" . $intervention->id . "'")->fetchAll(PDO::FETCH_OBJ);
		foreach($old_diligences as $old_diligence)
			foreach($diligences[$user->id] as $diligence)
				if ($diligence->old_id == $old_diligence->id)
					$diligences_id[] = (int)$diligence->id;
		
		//if (!$client->siren)
			//rest('https://api.' . $destination_host . '/optimus-contacts/' . $owner->id . '/contacts/' . $client->id, 'PATCH', '{"siren":"999999999"}', $token);

		$facture->number = $facture->numero;

		$facture->vat_rate = $tva_rates[$facture->tva];
		$facture->collectible = ($facture->irrecouvrable == 0) ? 1 : 0;
		$facture->template = 'Modèle 1.0 (français).odt';
		
		$facture->client_endpoint = '/optimus-contacts/{owner}/contacts/{id}';
		$facture->client_server = $destination_host;
		$facture->client_owner = $owner->id;
		$facture->client_id = (int)$client->id;

		$facture->details_endpoint = '/optimus-avocats/{owner}/dossiers/{reference}/diligences';
		$facture->details_server = $destination_host;
		$facture->details_owner = $owner->id;
		$facture->details_ids = $diligences_id;

		$facture->reference_endpoint = '/optimus-avocats/{owner}/dossiers/{id}';
		$facture->reference_server = $destination_host;
		$facture->reference_owner = $owner->id;
		$facture->reference_id = $reference->id;

		$facture->properties = null;

		$facture->notes = $facture->notes;

		$new_facture = rest('https://api.' . $destination_host . '/optimus-business/' . $businesses[$i]->id . '/invoices', 'POST', json_encode($facture), $token)->data;
		$new->query("UPDATE user_" . $businesses[$i]->id . ".invoices SET number = '" . $facture->numero . "' WHERE id = '" . $new_facture->id . "'")->fetch(PDO::FETCH_OBJ);
		$new->query("UPDATE user_" . $businesses[$i]->id . ".invoices SET date = '" . $facture->date . "' WHERE id = '" . $new_facture->id . "'")->fetch(PDO::FETCH_OBJ);

		if($facture->reminder1)
			rest('https://api.' . $destination_host . '/optimus-business/' . $businesses[$i]->id . '/invoices/' . $new_facture->id . '/reminders', 'POST', '{"date":"' . $facture->reminder1 . '"}', $token);
		if($facture->reminder2)
			rest('https://api.' . $destination_host . '/optimus-business/' . $businesses[$i]->id . '/invoices/' . $new_facture->id . '/reminders', 'POST', '{"date":"' . $facture->reminder2 . '"}', $token);
		if($facture->reminder3)
			rest('https://api.' . $destination_host . '/optimus-business/' . $businesses[$i]->id . '/invoices/' . $new_facture->id . '/reminders', 'POST', '{"date":"' . $facture->reminder3 . '"}', $token);
		if($facture->reminder4)
			rest('https://api.' . $destination_host . '/optimus-business/' . $businesses[$i]->id . '/invoices/' . $new_facture->id . '/reminders', 'POST', '{"date":"' . $facture->reminder4 . '"}', $token);
		if($facture->reminder5)
			rest('https://api.' . $destination_host . '/optimus-business/' . $businesses[$i]->id . '/invoices/' . $new_facture->id . '/reminders', 'POST', '{"date":"' . $facture->reminder5 . '"}', $token);
	}
}


//COPIE DES COURRIELS
clog("\n==COPIE DES COURRIELS==", "magenta");
clog("Appuyez une touche pour continuer, ou CTRL+C pour stopper le script... \n");
$handle = fopen ("php://stdin","r");
$line = fgets($handle);

ssh('sudo chmod +777 -R /srv/mailboxes');

$all_users = array_merge($users, $businesses);
foreach($all_users as &$user)
	if (!isset($user->replicant))
	{
		clog($user->email, 'cyan');

		ssh("sudo rm -f -R '/srv/mailboxes/gpg-keys/" . $user->email . "'");
		exec("sudo scp -p -r -P 7822 -i /root/.ssh/id_rsa '/srv/mailboxes/gpg-keys/" . $user->old_email . "' 'debian@" . $destination_host . ":/srv/mailboxes/gpg-keys/" . $user->email . "' >&2");
		
		exec("sudo scp -p -r -P 7822 -i /root/.ssh/id_rsa '/srv/mailboxes/" . $user->old_email . "' 'debian@" . $destination_host . ":/srv/mailboxes/temp' >&2");
		ssh("sudo cp -a '/srv/mailboxes/" . $user->email . "/INBOX/cur/.' '/srv/mailboxes/temp/INBOX/cur/' 2>/dev/null");
		ssh("sudo cp -a '/srv/mailboxes/" . $user->email . "/INBOX/new/.' '/srv/mailboxes/temp/INBOX/new/' 2>/dev/null");
		ssh("sudo rm -R '/srv/mailboxes/" . $user->email . "'");
		ssh("sudo mv '/srv/mailboxes/temp' '/srv/mailboxes/" . $user->email . "'");
		
		ssh("sudo chown -R mailboxes:mailboxes /srv/mailboxes/" . $user->email);
		ssh("sudo chmod -R u+rwX,g+rwX,o-rwx /srv/mailboxes/" . $user->email);
		
		clog("Source      : " . explode('	', exec('du -sh /srv/mailboxes/' . $user->old_email))[0] . ' dans ' . exec('find /srv/mailboxes/' . $user->old_email . ' -type f | wc -l') . ' fichiers', 'cyan');
		clog("Destination : " . explode('	', ssh("sudo du -sh /srv/mailboxes/" . $user->email)[0])[0] . " dans " . ssh("sudo find /srv/mailboxes/" . $user->email . " -type f | wc -l")[0] . " fichiers", 'cyan');
		
		ssh("docker exec optimus-mail doveadm mailbox rename -u " . $user->email . " '==DOSSIERS==' 'dossiers'");
		
		echo "\n";
	}

ssh("sudo chown www-data:mailboxes /srv/mailboxes");
ssh("sudo chmod 770 /srv/mailboxes");
ssh("sudo chown -R www-data:mailboxes /srv/mailboxes/gpg-keys");
ssh("sudo chmod -R u+rwX,g+rwX,o-rwx /srv/mailboxes/gpg-keys");


//COPIE DES FICHIERS
clog("\n==COPIE DES FICHIERS==", "magenta");
clog("Appuyez une touche pour continuer, ou CTRL+C pour stopper le script... \n");
$handle = fopen ("php://stdin","r");
$line = fgets($handle);

ssh("sudo mv /srv/files /srv/temp_files");
ssh("sudo mkdir /srv/files");
ssh("sudo chmod +777 -R /srv/files");

exec("sudo scp -p -r -P 7822 -i /root/.ssh/id_rsa '/srv/files' 'debian@" . $destination_host . ":/srv' >&2");

$all_users = array_merge($users, $businesses);
foreach($users as &$user)
{
	if (!isset($user->replicant))
	{
		ssh("sudo mv '/srv/files/" . $user->old_email . "' '/srv/files/" . $user->email . "'");
		clog("Source      : " . explode('	', exec('du -sh /srv/files/' . $user->old_email))[0] . ' dans ' . exec('find /srv/files/' . $user->old_email . ' -type f | wc -l') . ' fichiers', 'cyan');
		clog("Destination : " . explode('	', ssh('sudo du -sh /srv/files/' . $user->email)[0])[0] . ' dans ' . ssh('sudo find /srv/files/' . $user->email . ' -type f | wc -l')[0] . ' fichiers', 'cyan');
		ssh("sudo mv '/srv/files/" . $user->email . "/==DOSSIERS==' '/srv/files/" . $user->email . "/dossiers'");
	}
	else
	{
		ssh("sudo mkdir /srv/files/" . $user->email . "/ancien_modeles");
		exec("sudo scp -p -r -P 7822 -i /root/.ssh/id_rsa '/srv/files/" . $user->old_email . "/==MODELES==/Factures' 'debian@" . $destination_host . ":/srv/files/" . $user->email . "/ancien_modeles' >&2");
	}
	ssh("sudo mv '/srv/temp_files/" . $user->email . "/modeles' '/srv/files/" . $user->email . "/.' 2>/dev/null");
	ssh("sudo rm -R /srv/temp_files");
}


ssh("sudo chown -R www-data:www-data /srv/files");
ssh("sudo chmod -R u+rwX,g+rX,o-rwx /srv/files");

//SUPPRESSION DE L'ADMINISTRATEUR PROVISOIRE
clog("\n==SUPPRESSION DE L'ADMINISTRATEUR PROVISOIRE==", "magenta");
$new->query("DELETE FROM server.users WHERE id = 0");

//SUPRESSION DE LA CLE SSH
clog("\n==SUPPRESSION DE LA CLE SSH==", "magenta");
ssh("sudo sed -i '/" . explode(' ', exec('cat /root/.ssh/id_rsa.pub'))[2] . "/d' /root/.ssh/authorized_keys");

//REDEMARRAGE DES SERVICES
clog("\n==REDEMARRAGE DES SERVICES==", "magenta");
ssh("sudo docker restart optimus-drive");
ssh("sudo docker restart optimus-business");
ssh("sudo docker restart optimus-mail");

echo "\n"
?>